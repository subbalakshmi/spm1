'use strict';

var express = require("express");
var app = express();

app.use("/src",express.static("src"));
app.use("/bower_components", express.static("bower_components"));

var port = process.env.PORT || 8080;

app.listen(8080, function () {
    console.log("Server running on port :: " + port);
});