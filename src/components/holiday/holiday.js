(function() {
    'use strict';

    angular.module('SPM.holiday', [])
        .config(routeConfig)
        .controller('holidayCtrl', routeController);

    function routeConfig($stateProvider) {
        $stateProvider.state('holiday', {
            url: '/holiday',
            templateUrl: 'components/holiday/holiday.html',
            controller: "holidayCtrl"
        })
    }

    function routeController() {

    }
})();
