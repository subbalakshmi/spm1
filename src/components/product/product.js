(function() {
    'use strict';

    angular.module('SPM.product', [])
        .config(routeConfig)
        .controller('productCtrl', routeController);

    function routeConfig($stateProvider) {
        $stateProvider.state('product', {
            url: '/product',
            templateUrl: 'components/product/product.html',
            controller: "productCtrl"
        })
    }

    function routeController() {

    }
})();
