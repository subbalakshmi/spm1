(function() {
    'use strict';

    angular.module('SPM.ifsc', [])
        .config(routeConfig)
        .controller('ifscCtrl', routeController);

    function routeConfig($stateProvider) {
        $stateProvider.state('ifsc', {
            url: '/ifsc',
            templateUrl: 'components/ifsc/ifsc.html',
            controller: "ifscCtrl"
        })
    }

    function routeController() {

    }
})();
